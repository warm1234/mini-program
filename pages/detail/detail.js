// pages/detail/detail.js
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        itemsPicture: '',
        itemsName: '',
        itemsPrice: '',
        itemsNumber: '',
        remark: '',
        itemsPublisher: '',
        itemsPhoneNumber: '',

        // input默认是1 
        num: 1,
        // 使用data数据对象设置样式名 
        minusStatus: 'disabled',
        maxStatus: 'normal'
    },

    /* 点击减号 */
    bindMinus: function () {
        var num = this.data.num;
        // 如果大于1时，才可以减 
        if (num > 1) {
            num--;
        }
        // 只有大于一件的时候，才能normal状态，否则disable状态 
        var minusStatus = num <= 1 ? 'disabled' : 'normal';
        var maxStatus = num >= this.data.itemsNumber ? 'disabled' : 'normal';
        // 将数值与状态写回 
        this.setData({
            num: num,
            minusStatus: minusStatus,
            maxStatus: maxStatus
        });
    },
    /* 点击加号 */
    bindPlus: function () {
        var num = this.data.num;
        if(num < this.data.itemsNumber){
            num++;
        }
        // 不作过多考虑自增1 
        // 只有大于一件的时候，才能normal状态，否则disable状态 
        var minusStatus = num <= 1 ? 'disabled' : 'normal';
        var maxStatus = num >= this.data.itemsNumber ? 'disabled' : 'normal';
        // 将数值与状态写回 
        this.setData({
            num: num,
            minusStatus: minusStatus,
            maxStatus: maxStatus
        });
    },
    /* 输入框事件 */
    bindManual: function (e) {
        var num = e.detail.value;
        // 将数值与状态写回 
        this.setData({
            num: num
        });
    },

    orderCreate(){
        var itemsID = wx.getStorageSync('itemsID')
        console.log(itemsID)
        var that = this;
        wx.request({
            url: dataUrl+'items/'+itemsID+'/',
            data: {
                itemsID : itemsID,
                buyNum: this.data.num
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'POST',
            dataType: 'json',
            success (res) {
                console.log(res.data)
                wx.setStorageSync('orderID', res.data.data)
                wx.navigateTo({
                  url: '/pages/orderDetail/index',
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // console.log(this.data.itemsPicture)
        var itemsID = wx.getStorageSync('itemsID')
        console.log(itemsID)
        var that =this;
        wx.request({
            url: dataUrl+'items/'+itemsID+'/',
            data: {
                itemsID : itemsID
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'GET',
            dataType: 'json',
            success (res) {
                console.log(res.data.data)
                var data = res.data.data;
                var itemsStartTime = data.itemsStartTime.slice(0,10)
                var itemsEndTime = data.itemsEndTime.slice(0,10)
                that.setData({
                    itemsName: data.itemsName,
                    itemsPrice: data.itemsPrice,
                    itemsNumber: data.itemsNumber,
                    remark: data.remark,
                    itemsPublisher: data.itemsPublisher,
                    // itemsPhoneNumber: data.
                    itemsPicture: data.itemsPicture,
                    itemsStartTime: itemsStartTime,
                    itemsEndTime: itemsEndTime
                })

            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
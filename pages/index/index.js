//index.js
//获取应用实例
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()

const app = getApp()

Page({
  data: {
    //tabbar
    tabbar: {},
    itemsList: [],
    currtab: 0,

    typecode: 0,

    goodsList: [],
    goodsList1: [],
    goodsList2: [],
    goodsList3: [],
    //首页轮播图
    imgUrls: [
      "/images/mine/1.jpg",
      "/images/mine/2.jpg",
      "/images/mine/3.jpg",
      "/images/mine/4.jpg"
    ],
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 3000, //自动切换时间间隔
    duration: 1000 //滑动动画时长
  },

  onLoad: function () {
    wx.hideTabBar();
    app.editTabbar();
    

  },
  onShow: function () {
    this.getgoodsList();

  },
  toDetail(e){
    let index = e.currentTarget.dataset.index
    // console.log(this.data.goodsList[index].itemsID)
    var itemsID = this.data.goodsList[index].itemsID
    // var itemsID = itemsID.replace('-','')
    console.log(itemsID)
    wx.setStorageSync('itemsID', itemsID)

    wx.navigateTo({
      url: '/pages/detail/detail',
    })
    
},
  getgoodsList() {
    var that = this;
    wx.request({
      url: dataUrl + 'items/',
      data: {
        typecode: 0,
        limit: 10000000,
        page: 1
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        if (res.data.code == 200) {

          that.setData({
            goodsList: res.data.data
          })
          // console.log(that.data.goodsList[1].itemsPicture)
        } else if (res.data.code == 101) {
          wx.showToast({
            title: res.data.msg,
            icon: 'error',
            duration: 1000
          })
        }
      }
    })
  },
  getgoodsList1() {
    var that = this;
    wx.request({
      url: dataUrl + 'items/',
      data: {
        typecode: 1,
        limit: 10000000,
        page: 1
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        if (res.data.code == 200) {
          that.setData({
            goodsList1: res.data.data
          })
        } else if (res.data.code == 101) {
          wx.showToast({
            title: res.data.msg,
            icon: 'error',
            duration: 1000
          })
        }
      }
    })
  },
  getgoodsList2() {
    var that = this;
    wx.request({
      url: dataUrl + 'items/',
      data: {
        typecode: 2,
        limit: 10000000,
        page: 1
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        if (res.data.code == 200) {
          that.setData({
            goodsList2: res.data.data
          })
        } else if (res.data.code == 101) {
          wx.showToast({
            title: res.data.msg,
            icon: 'error',
            duration: 1000
          })
        }
      }
    })
  },
  getgoodsList3() {
    var that = this;
    wx.request({
      url: dataUrl + 'items/',
      data: {
        typecode: 3,
        limit: 10000000,
        page: 1
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        if (res.data.code == 200) {
          that.setData({
            goodsList3: res.data.data
          })
        } else if (res.data.code == 101) {
          wx.showToast({
            title: res.data.msg,
            icon: 'error',
            duration: 1000
          })
        }
      }
    })
  },

  search() {
    wx.navigateTo({
      url: '/pages/search/index',
    })
  },
  type0() {
    this.setData({
      typecode: 0
    })
  },
  type1() {
    this.setData({
      typecode: 1
    })
    this.getgoodsList1();
  },
  type2() {
    this.setData({
      typecode: 2
    })
    this.getgoodsList2();

  },
  type3() {
    this.setData({
      typecode: 3
    })
    this.getgoodsList3();

  },


})
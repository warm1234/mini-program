// pages/login/login.js
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        state: false,
        phoneNumber: '',
        password: '',
        password2:'',
        stuID: '',
        nickname: '',
        userPro: '',
        userAns: '',
    },
    phoneNumberInput: function (e) {
        this.setData({
            phoneNumber: e.detail.value
        })

    },

    pwdBlur: function (e) {
        this.setData({
            password: e.detail.value
        });

    },
    pwd2Blur(e){
        this.setData({
            password2: e.detail.value
        })
    },
    stuIDInput: function (e) {
        this.setData({
            stuID: e.detail.value
        });

    },
    nicknameInput: function (e) {
        this.setData({
            nickname: e.detail.value
        });

    },
    userProInput: function (e) {
        this.setData({
            userPro: e.detail.value
        });

    },
    userAnsInput: function (e) {
        this.setData({
            userAns: e.detail.value
        });

    },
    zhuce: function (e) {
        this.setData({
            state: true
        })
    },
    denglu: function (e) {
        this.setData({
            state: false
        })
    },
    mbwt(){
        // wx.request({
        //   url: 'url',
        // })
        wx.navigateTo({
          url: '../refind/refind',
        })
    },
    login: function (e) {
        // app.globalData.Login = true
        // wx.switchTab({

        //     url: '../index/index'
        // })
        // console.log(app.globalData.Login)
        var that = this;
        if (!this.data.state) {

            wx.request({
                url: dataUrl + 'users/',
                data: {
                    typecode: 0,
                    phoneNumber: this.data.phoneNumber,
                    password: this.data.password
                },
                header: {
                    'content-type': 'application/json' // 默认值
                },
                method: 'GET',
                dataType: 'json',
                success(res) {
                    console.log(res)
                    if (res.data.code == 200) {
                        app.globalData.Login = true
                        app.globalData.phoneNumber = that.data.phoneNumber
                        //app.globalData.sessionid = res.cookies[0]
                        var ans = res.cookies[0].split(';')
                        app.globalData.sessionid = ans[0];
                        // console.log(sessionid)
                        wx.switchTab({

                            url: '../index/index'
                        })
                    } else if (res.data.code == 101) {
                        wx.showToast({
                            title: res.data.msg,
                            icon: 'none',
                            duration: 1000
                        })
                    }
                }
            })

        } else {
            wx.request({
                url: dataUrl + 'users/',
                data: {
                    phoneNumber: this.data.phoneNumber,
                    password: this.data.password,
                    password2:this.data.password2,
                    nickname: this.data.nickname,
                    stuID: this.data.stuID,
                    userPro: this.data.userPro,
                    userAns: this.data.userAns
                },
                header: {
                    'content-type': 'application/json' // 默认值
                },
                method: 'POST',
                dataType: 'json',
                success(res) {
                    console.log(res.data)
                    if (res.data.code == 200) {

                        wx.showToast({
                            title: '注册成功',
                            duration: 1000
                        })

                        that.setData({
                            state: false,
                            phoneNumber: '',
                            password: '',
                            stuID: '',
                            nickname: '',
                            userPro: '',
                            userAns: '',
                        })
                    } else if (res.data.code == 101) {
                        wx.showToast({
                            title: res.data.msg,
                            icon: 'none',
                            duration: 1000
                        })
                    }
                }
            })

        }
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {},

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
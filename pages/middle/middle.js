// pages/middle/middle.js
//引入事先写好的日期设置util.js文件
var datePicker = require('../../utils/dateSetting.js')
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()

//设定当前的时间，将其设定为常量
const date = new Date();
const year = date.getFullYear();
const month = date.getMonth() + 1;

const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

    now: '',

    //导航栏的数据
    postPublic: true,
    postPrivateThing: false,
    postPrivateTalent: false,

    itemsType: 0,
    itemsTypeEnd: ['公共资源','个人资源','个人能力'],

    phoneNumber: '',
    itemsName: '',
    itemsPicture: '',
    itemsPrice: '',
    itemsMaxNum: '',
    itemsStartTime: '',
    itemsEndTime: '',
    remark: '',
    // campus:["北洋园校区","卫津路校区"],
    // campusIndex: 0,
    // address:'',

  },

  bindStartTimeChange(e) {
    this.setData({
      itemsStartTime: e.detail.value
    })
    if (this.data.itemsEndTime <= this.data.itemsStartTime) {
      wx.showToast({
        title: '结束时间需要在起始时间之后',
        icon: 'none'
      })
    }
  },
  bindEndTimeChange(e) {
    this.setData({
      itemsEndTime: e.detail.value
    })
    if (this.data.itemsEndTime <= this.data.itemsStartTime) {
      wx.showToast({
        title: '结束时间需要在起始时间之后',
        icon: 'none'
      })
    }
  },

  //导航栏的响应事件
  choosePostPublic: function (e) {
    this.setData({
      postPublic: true,
      postPrivateThing: false,
      postPrivateTalent: false,
      itemsType: 0
    })
  },
  choosePostPrivateThing: function (e) {
    this.setData({
      postPublic: false,
      postPrivateThing: true,
      postPrivateTalent: false,
      itemsType: 1
    })
  },
  choosePostPrivateTalent: function (e) {
    this.setData({
      postPublic: false,
      postPrivateThing: false,
      postPrivateTalent: true,
      itemsType: 2
    })
  },
  bindImageInput: function () {
    var that = this;
    wx.chooseImage({
      count: 1,
      sourceType: ['album', 'camera'],
      success: function (res) {
        var bookImg1 = res.tempFilePaths[0];
        console.log(bookImg1);
        that.setData({
          itemsPicture: bookImg1
        })
      },

    })
  },
  bindCampusChange: function (e) {
    this.setData({
      campusIndex: e.detail.value
    })
  },
  bindNameInput: function (e) {
    this.setData({
      itemsName: e.detail.value
    })
  },
  bindNumberInput: function (e) {
    this.setData({
      phoneNumber: e.detail.value
    })

  },
  bindPriceInput: function (e) {
    this.setData({
      itemsPrice: e.detail.value
    })
  },
  bindNumInput: function (e) {
    this.setData({
      itemsMaxNum: e.detail.value
    })
  },
  bindRemarkInput: function (e) {
    this.setData({
      remark: e.detail.value
    })
  },
  bindSubmit: function (e) {
    // console.log(this.data.itemsPicture)
    wx.request({
      url: dataUrl +  'users/0/items/',
      data: {
        itemsType: this.data.itemsTypeEnd[this.data.itemsType],
        itemsName: this.data.itemsName,
        itemsPrice: this.data.itemsPrice,
        itemsMaxNum: this.data.itemsMaxNum,
        itemsPicture: this.data.itemsPicture,
        itemsStartTime: this.data.itemsStartTime,
        itemsEndTime: this.data.itemsEndTime,
        remark: this.data.remark
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'POST',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        if (res.data.code == 200) {
          wx.showToast({
            title: '发布成功',
            duration: 1000
          })
          wx.switchTab({
            url: '../index/index',
          })
        } else if (res.data.code == 101) {
          wx.showToast({
            title: res.data.msg,
            icon: 'error',
            duration: 1000
          })
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      now: year + '-' + datePicker.determineMonth()[0] + '-' + datePicker.determineDay(year, month)[0],
      phoneNumber: app.globalData.phoneNumber
    })

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
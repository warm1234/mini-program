// pages/mine/mine.js
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()

const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    //tabbar
    tabbar: {},
    Login: app.globalData.Login,
    nickname:'',
    stuID:'',
    channel:
    [
      {
        name:"我的发布",
        imgurl:"../../images/mine/fb.png",
        url:"../mineMore/resources/resources"
      },
      {
        name:"我的订单",
        imgurl:"../../images/mine/buy.png",
        url:"../mineMore/myOrder/myOrder"
      },
      {
        name:"信息修改",
        imgurl:"../../images/mine/xiugai.png",
        url:"../mineMore/MyData/Revise"
      },
      
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    wx.hideTabBar();
    app.editTabbar();
    
  },

  getUserData: function(e){
    wx.navigateTo({
      url: '/pages/mineMore/basicData/basicData',
    })
  },
  bindLogin(){
    wx.navigateTo({
      url: '../login/login',
    })
  },
  bindClear(){
    
    wx.request({
      url: dataUrl+'users/',
      data: {
          typecode: 1,
      },
      header: {
          'content-type': 'application/json' // 默认值
      },
      method: 'GET',
      dataType: 'json',
      success (res) {
          console.log(res.data)
          if(res.data.code == 200)
          {
              app.globalData.Login = false
              app.globalData.phoneNumber = ''
              app.globalData.stuID = ''
              app.globalData.nickname = ''
              wx.switchTab({

                  url: '../index/index'
              })
          }
          else if(res.data.code == 101)
          {
              wx.showToast({
                title: res.data.msg,
                icon:'error',
                duration: 1000
              })
          }
      }
  })
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      Login: app.globalData.Login
    })
    if(app.globalData.Login){
    this.getData();
  }
  },

  getData(){
    var that = this;
    wx.request({
      url: dataUrl + 'users/0/',
      data: {
      },
      header: {
          'content-type': 'application/json', // 默认值
          'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
          console.log(res.data)
          if (res.data.code == 200) {
            app.globalData.nickname = res.data.data.nickname,
            app.globalData.stuID = res.data.data.stuID
            console.log(app.globalData)
            that.setData({
              nickname:app.globalData.nickname,
              stuID:app.globalData.stuID
            })
            // sessionid: app.globalData.sessionid
              // app.globalData.Login = true
              // app.globalData.userInfo.phoneNumber = that.data.phoneNumber
          } else if (res.data.code == 101) {
              wx.showToast({
                  title: res.data.msg,
                  icon: 'error',
                  duration: 1000
              })
          }
      }
  })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
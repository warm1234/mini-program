// pages/mineMore/MyData/Revise.js
var Data = require('../../../utils/data.js')
var dataUrl = Data.getUrl()

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        password:'',
        password2:'',
        phoneNumber: app.globalData.phoneNumber,
        stuID: app.globalData.stuID,
        nickname: app.globalData.nickname,
        newPassword:'',
        code:true
    },
    pwdBlur(e){
        this.setData({
            password:e.detail.value
        })
    },
    newpwd2Blur(e){
        this.setData({
            password2:e.detail.value
        })
    },
    phnumBlur(e){
        this.setData({
            phoneNumber:e.detail.value
        })
    },
    stuIDBlur(e){
        this.setData({
            stuID:e.detail.value
        })
    },
    nickNameBlur(e){
        this.setData({
            nickname:e.detail.value
        })
    },
    newpwdBlur(e){
        this.setData({
            newPassword:e.detail.value
        })
    },
    mbwt(){
        this.setData({
            code: false
        })
    },
    tz(){
        this.setData({
            code:true
        })
    },

    confirm(){
        wx.request({
            url: dataUrl+'users/0/',
            data:{
                password:this.data.password,
                newPhoneNumber:this.data.phoneNumber,
                stuID: this.data.stuID,
                nickname: this.data.nickname,
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'PUT',
            dataType: 'json',
            success(res){
                console.log(res.data)
                if(res.data.code == 200)
                {
                    // app.globalData.userInfo.phoneNumber = 
                    // app.globalData.userInfo.stuID = 
                    // app.globalData.userInfo.nickname = 
                    wx.switchTab({

                        url: '../../mine/mine'
                    })
                }
                else if(res.data.code == 101)
                {
                    wx.showToast({
                        title: res.data.msg,
                        icon:'error',
                        duration: 1000
                    })
                }
            }
        })
    },

    pwdConfirm(){
        // console.log('?')
        wx.request({
            url: dataUrl+'users/0/',
            data:{
                typecode:1,
                password: this.data.password,
                newPassword:this.data.newPassword,
                password2: this.data.password2
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'POST',
            dataType: 'json',
            success(res){
                console.log(res.data)
                if(res.data.code == 200)
                {  
                    wx.showToast({
                        title: "修改密码成功",
                        duration: 1000
                    })
                    wx.navigateTo({

                        url: '/pages/login/login'
                    })
                }
                else if(res.data.code == 101)
                {
                    wx.showToast({
                        title: res.data.msg,
                        icon:'error',
                        duration: 1000
                    })
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            phoneNumber: app.globalData.phoneNumber,
            stuID: app.globalData.stuID,
            nickname: app.globalData.nickname,
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
var Data = require('../../../utils/data.js')
var dataUrl = Data.getUrl()
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currtab: 0,
    swipertab: [{
        name: '全部',
        index: 0
      }, {
        name: '待付款',
        index: 1
      }, {
        name: '待审批',
        index: 2
      },
      {
        name: '审批',
        index: 3
      }
    ],
    orderlist: [],
    orderlist1: [],
    orderlist2: [],
    orderlist3: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAll();
    this.get1();
    this.get2();
    this.get3();

  },

  toDetail(e) {
    let index = e.currentTarget.dataset.index
    var orderID;
    // console.log(this.data.goodsList[index].itemsID)
    if (this.data.currtab == 0) {
      orderID = this.data.orderlist[index].orderID
    } else if (this.data.currtab == 1) {
      orderID = this.data.orderlist1[index].orderID
    } else if (this.data.currtab == 2) {
      orderID = this.data.orderlist2[index].orderID
    } else {
      orderID = this.data.orderlist3[index].orderID
    }
    // var itemsID = itemsID.replace('-','')
    console.log(orderID)
    wx.setStorageSync('orderID', orderID)

    wx.navigateTo({
      url: '/pages/orderDetail/index',
    })

  },

  getAll() {
    var that = this;
    wx.request({
      url: dataUrl + 'users/0/orders/',
      data: {
        type: 0,
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        that.setData({
          orderlist: res.data.data
        })

      }
    })
  },

  get1() {
    var that = this;

    wx.request({
      url: dataUrl + 'users/0/orders/',
      data: {
        type: 1,
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        that.setData({
          orderlist1: res.data.data
        })
        console.log(res.data)
      }
    })
  },

  get2() {
    var that = this;

    wx.request({
      url: dataUrl + 'users/0/orders/',
      data: {
        type: 2,
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        that.setData({
          orderlist2: res.data.data
        })
        console.log(res.data)
      }
    })
  },

  get3() {
    var that = this;

    wx.request({
      url: dataUrl + 'users/0/orders/',
      data: {
        type: 3,
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'GET',
      dataType: 'json',
      success(res) {
        console.log(res.data)
        that.setData({
          orderlist3: res.data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 页面渲染完成
    this.getDeviceInfo()
  },

  zhifu(e) {
    let index = e.currentTarget.dataset.index
    var orderID = this.data.orderlist1[index].orderID;
    wx.request({
      url: dataUrl + 'users/0/orders/' + orderID + '/',
      data: {
        type: '确认',
        orderID: orderID
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'PUT',
      dataType: 'json',
      success(res) {
        console.log(res.data.data)
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
    })
  },

  successPass(e) {
    let index = e.currentTarget.dataset.index
    var orderID = this.data.orderlist3[index].orderID;
    var that = this;

    wx.request({
      url: dataUrl + 'users/0/orders/' + orderID + '/',
      data: {
        type: '审批',
        orderID: orderID
      },
      header: {
        'content-type': 'application/json', // 默认值
        'Cookie': app.globalData.sessionid
      },
      method: 'PUT',
      dataType: 'json',
      success(res) {
        console.log(res.data.data)
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
    })
  },

  getDeviceInfo: function () {
    let that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          deviceW: res.windowWidth,
          deviceH: res.windowHeight
        })
      }
    })
  },

  /**
   * @Explain：选项卡点击切换
   */
  tabSwitch: function (e) {
    // console.log(e)
    var that = this
    if (this.data.currtab === e.target.dataset.current) {
      return false
    } else {
      that.setData({
        currtab: e.target.dataset.current
      })
    }
  },

  tabChange: function (e) {
    // console.log(e)
    this.setData({
      currtab: e.detail.current
    })
  },

})
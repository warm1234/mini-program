var Data = require('../../../utils/data.js')
var dataUrl = Data.getUrl()
const app = getApp();
Page({
 
    /**
     * 页面的初始数据
     */
    data: {
      index: '',
      currtab: 0,
      swipertab: [{ name: '上架中', index: 0 }, { name: '已售罄', index: 1 }],
      goodsList:'',
      goodsList1:'',
    },
   
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      this.shangjia();

        this.selled();
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
      // 页面渲染完成
      this.getDeviceInfo()
    },

    shangjia(){
      var that =this;
        wx.request({
            url: dataUrl+'users/0/items/',
            data: {
                typecode:'上架中',
                limit: 100000,
                page:1
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'GET',
            dataType: 'json',
            success (res) {
                console.log(res.data.data)
                var data = res.data.data;
                that.setData({
                  goodsList: data
                })

            }
        })
    },

    selled(){
      var that =this;
        wx.request({
            url: dataUrl+'users/0/items/',
            data: {
                typecode:'已售罄',
                limit: 100000,
                page:1
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'GET',
            dataType: 'json',
            success (res) {
                console.log(res.data.data)
                var data = res.data.data;
                that.setData({
                  goodsList1: data
                })

            }
        })
    },
   
    getDeviceInfo: function () {
      let that = this
      wx.getSystemInfo({
        success: function (res) {
          that.setData({
            deviceW: res.windowWidth,
            deviceH: res.windowHeight
          })
        }
      })
    },

    toDetail(e){
      let index = e.currentTarget.dataset.index
      var itemsID;
      // console.log(this.data.goodsList[index].itemsID)
      if(this.data.currtab == 0)
        itemsID = this.data.goodsList[index].itemsID
      else
        itemsID = this.data.goodsList1[index].itemsID
      console.log(itemsID)
      wx.setStorageSync('itemsID', itemsID)

      wx.navigateTo({
        url: '/pages/detail/detail',
      })
      
  },

    tzzy(e){
      let index = e.currentTarget.dataset.index
      // console.log(this.data.goodsList[index].itemsID)
      var itemsID = this.data.goodsList[index].itemsID
      wx.request({
        url: dataUrl+'users/0/items/'+itemsID+'/',
        data: {
            itemsID: itemsID
        },
        header: {
            'content-type': 'application/json', // 默认值
            'Cookie': app.globalData.sessionid
        },
        method: 'DELETE',
        dataType: 'json',
        success (res) {
            console.log(res.data.data)
            wx.switchTab({
              url: '/pages/index/index',
            })
        }
    })
    },
   
    /**
    * @Explain：选项卡点击切换
    */
    tabSwitch: function (e) {
        // console.log(e)
      var that = this
      if (this.data.currtab === e.target.dataset.current) {
        return false
      } else {
        that.setData({
          currtab: e.target.dataset.current
        })
      }
    },
   
    tabChange: function (e) {
        // console.log(e)
      this.setData({ currtab: e.detail.current })
    },
   
    
  })
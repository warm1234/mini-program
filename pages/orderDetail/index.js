// pages/orderDetail/index.js
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()

const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list:[]
    },

    cancal(){
        var orderID = wx.getStorageSync('orderID')
        wx.request({
            url: dataUrl+'users/0/orders/'+orderID+'/',
            data: {
                type: '取消',
                orderID: orderID
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'PUT',
            dataType: 'json',
            success (res) {
                console.log(res.data.data)
                wx.switchTab({
                  url: '/pages/index/index',
                })
            }
        })
    },
    confirm(){
        var orderID = wx.getStorageSync('orderID')
        wx.request({
            url: dataUrl+'users/0/orders/'+orderID+'/',
            data: {
                type: '确认',
                orderID: orderID
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'PUT',
            dataType: 'json',
            success (res) {
                console.log(res.data.data)
                wx.switchTab({
                    url: '/pages/index/index',
                  })
            }
        })
    },

    getOrder(){
        var orderID = wx.getStorageSync('orderID')
        var that =this
        console.log(orderID)
        wx.request({
            url: dataUrl+'users/0/orders/'+orderID+'/',
            data: {
                orderID: orderID
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'GET',
            dataType: 'json',
            success (res) {
                that.setData({
                    list: res.data.data
                })
                console.log(res.data)
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getOrder();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
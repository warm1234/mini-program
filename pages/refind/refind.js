// pages/refind/refind.js
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phoneNumber:'',
        userPro:'??',
        userAns:'',
        Yes:false,
        pwd1:'',
        pwd2:'',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // wx.request({
        //   url: 'url',
        // })
    },
    phNumInput(e){
        this.setData({
            phoneNumber: e.detail.value
        })
    },
    getAns(){
      var that = this;
        wx.request({
            url: dataUrl + 'users/0/',
            data: {
              typecode: 0,
              username: this.data.phoneNumber
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            method: 'POST',
            dataType: 'json',
            success(res) {
            //   console.log(res.data)
              if (res.data.code == 200) {
                that.setData({
                  userPro: res.data.data,
                  Yes: true
                })
                console.log(res.data)
              } else if (res.data.code == 101) {
                wx.showToast({
                  title: res.data.msg,
                  icon: 'error',
                  duration: 1000
                })
              }
            }
          })
    },
    ansInput(e){
        this.setData({
            userAns:e.detail.value
        })
    },
    changePwd(){
      console.log('?')
        wx.request({
            url: dataUrl + 'users/0/',
            data: {
              typecode: 0,
              username: this.data.phoneNumber,
              userAns: this.data.userAns,
              newPassword: this.data.pwd1,
              Password2: this.data.pwd2
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            method: 'POST',
            dataType: 'json',
            success(res) {
            //   console.log(res.data)
              if (res.data.code == 200) {
                console.log(res.data)
                wx.showToast({
                  title: res.data.msg,
                  duration: 1000
                })
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              } else if (res.data.code == 101) {
                wx.showToast({
                  title: res.data.msg,
                  icon: 'error',
                  duration: 1000
                })
              }
            }
          })

    },
    pwd1Blur(e){
        this.setData({
            pwd1:e.detail.value
        })
    },
    pwd2Blur(e){
        this.setData({
            pwd2:e.detail.value
        })
    },
    change(){
        if(this.data.pwd1 != this.data.pwd2){
            wx.showToast({
              title: '输入两次密码不相同',
              icon: "none"
            })
        }
        else{
            // wx.request({
            //   url: 'url',
            // })
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
// pages/search/index.js
var Data = require('../../utils/data.js')
var dataUrl = Data.getUrl()
const app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        key:'',
        goodsList:[],
    },

    toDetail(e){
        let index = e.currentTarget.dataset.index
        // console.log(this.data.goodsList[index].itemsID)
        var itemsID = this.data.goodsList[index].itemsID
        console.log(itemsID)
        wx.setStorageSync('itemsID', itemsID)

        wx.navigateTo({
          url: '/pages/detail/detail',
        })
        
    },

    keyInput(e){
        this.setData({
            key: e.detail.value
        })
    },
    search(){
        var that =this;
        wx.request({
            url: dataUrl+'items/search/',
            data: {
                q: this.data.key
            },
            header: {
                'content-type': 'application/json', // 默认值
                'Cookie': app.globalData.sessionid
            },
            method: 'GET',
            dataType: 'json',
            success (res) {
                console.log(res.data)
                that.setData({
                    goodsList: res.data
                })

            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
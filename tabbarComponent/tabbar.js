// tabBarComponent/tabBar.js
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabbar: {
      type: Object,
      value: {
        "backgroundColor": "#ffffff",
        "color": "#979795",
        "selectedColor": "#1c1c1b",
        "list": [
          {
            "pagePath": "/pages/index/index",
            "iconPath": "icon/icon_home.png",
            "selectedIconPath": "icon/icon_home_HL.png",
            "text": "首页"
          },
          {
            "pagePath": "/pages/middle/middle",
            "iconPath": "icon/icon_release.png",
            "isSpecial": true,
            "text": "发布",
            // "Login":app.globalData.Login
          },
          {
            "pagePath": "/pages/mine/mine",
            "iconPath": "icon/icon_mine.png",
            "selectedIconPath": "icon/icon_mine_HL.png",
            "text": "我的"
          }
        ]
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isIphoneX: app.globalData.systemInfo.model.search('iPhone X') != -1 ? true : false ,
    // Login: app.globalData.Login,
    url:"/pages/login/login",
    url1:"/pages/middle/middle",
    Login: app.globalData.Login
  },

  /**
   * 组件的方法列表
   */
  methods: {

  },
  // onLoad: function () {
  //   this.setData({
  //     Login: app.globalData.Login
  //   })
  //   if(this.data.Login == false){
  //     this.setData({
  //       url : "../pages/login/login"
  //     })
      
  //   }
  //   else{
  //     this.setData({
  //       url : "/pages/middle/middle"
  //     })
  //   }
  //   console.log(this.data)
  // }
})
